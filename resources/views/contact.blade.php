<x-apps-layout>
        <div class="min-h-screen flex flex-col items-center p-6 sm:pt-0">
            <div class="w-full mt-6 shadow-md">
                @livewire('contact-us')
            </div>
        </div>
</x-apps-layout>
