<div class="relative bg-white">
    <div class="absolute inset-0">
        <div class="absolute inset-y-0 left-0 w-1/2 bg-gray-50"></div>
    </div>
    <div class="relative max-w-7xl mx-auto">
        <div class="bg-gray-50 py-16 px-4 sm:px-6 md:col-span-2 md:px-8 md:py-24 xl:pr-12">
            <div class="max-w-lg mx-auto">
                <h2 class="text-2xl font-extrabold tracking-tight text-gray-900 sm:text-3xl pt-4">
                    {{ __('Contact us') }}
                </h2>
                <dl class="space-y-10 md:space-y-12 md:grid md:grid-cols-2 md:gap-x-8 md:gap-y-12">
                    <div class="pt-8">
                        <dt class="text-lg leading-6 font-medium text-gray-900">
                            Χρειάζεστε νέα λειτουργία;
                        </dt>
                        <dd class="mt-2 text-base text-gray-500">
                            Γράψτε αναλυτικά την διαδικασία υπολογισμού. Εάν αφορά αρχεία τότε στείλτε email επισυνάπτοντας το αρχείο αρχικό αρχείο και το αντίστοιχο εξαγόμενο.
                        </dd>
                    </div>
                    <div class="pt-8">
                        <dt class="text-lg leading-6 font-medium text-gray-900">
                            Κάποια εφαρμογή δυσλειτουργεί;
                        </dt>
                        <dd class="mt-2 text-base text-gray-500">
                            Αναφέρετε την εφαρμογή και πώς προκύπτει το πρόβλημα.
                            Εάν πρόκειται για εφαρμογή με αρχεία στείλτε email επισυνάπτοντας το αρχικό αρχείο και το εξαγόμενο που λαμβάνετε.
                        </dd>
                    </div>
                    <div class="pt-8">
                        <dt class="text-lg leading-6 font-medium text-gray-900 ">
                            Κάτι άλλο;
                        </dt>
                        <dd class="mt-2 text-base text-gray-500">
                            Γράψτε τα σχόλιά σας.
                        </dd>
                    </div>
                    <div class="pt-8">
                        <dt class="text-lg leading-6 font-medium text-gray-900 ">
                            Είστε ικανοποιημένοι;
                        </dt>
                        <dd class="mt-2 text-base text-gray-500">
                            Γράψτε πώς σας βοήθησε η σελίδα.
                        </dd>
                    </div>
                </dl>
            </div>

            <dl class="mt-8 text-base text-gray-500">
                <div class="mt-3 ">
                    <dt class="sr-only">Email</dt>
                    <dd class="flex">
                        <svg class="flex-shrink-0 h-6 w-6 text-gray-400" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 8l7.89 5.26a2 2 0 002.22 0L21 8M5 19h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z"/>
                        </svg>
                        <span class="ml-3">
                            <a href="mailto:vardtheo@theograms.tech">
                                vardtheo@theograms.tech
                            </a>
                        </span>
                    </dd>
                </div>
                <div class="mt-3">
                    <dt class="sr-only">LinkedIn</dt>
                    <dd class="flex">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" stroke="currentColor">
                            <path d="M19 0h-14c-2.761 0-5 2.239-5 5v14c0 2.761 2.239 5 5 5h14c2.762 0 5-2.239 5-5v-14c0-2.761-2.238-5-5-5zm-11 19h-3v-11h3v11zm-1.5-12.268c-.966 0-1.75-.79-1.75-1.764s.784-1.764 1.75-1.764 1.75.79 1.75 1.764-.783 1.764-1.75 1.764zm13.5 12.268h-3v-5.604c0-3.368-4-3.113-4 0v5.604h-3v-11h3v1.765c1.396-2.586 7-2.777 7 2.476v6.759z"/>
                        </svg>
                        <span class="ml-3">
                            <a href="https://www.linkedin.com/in/theofanis-vardatsikos/" target="_blank">
                                Theofanis Vardatsikos
                            </a>
                        </span>
                    </dd>
                </div>
            </dl>

        </div>
    </div>

    <div class="bg-white py-16 px-4 sm:px-6 md:col-span-3 md:py-24 md:px-8 xl:pl-12">
        <div class="max-w-lg mx-auto md:max-w-none">
            <form action="#" method="POST" class="grid grid-cols-1 gap-y-6">
                <div>
                    <label for="full_name" class="sr-only">Full name</label>
                    <input type="text" name="full_name" id="full_name" autocomplete="name" class="block w-full shadow-sm py-3 px-4 placeholder-gray-500 focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded-md" placeholder="Full name">
                </div>
                <div>
                    <label for="email" class="sr-only">Email</label>
                    <input id="email" name="email" type="email" autocomplete="email" class="block w-full shadow-sm py-3 px-4 placeholder-gray-500 focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded-md" placeholder="Email">
                </div>
                <div>
                    <label for="phone" class="sr-only">Phone</label>
                    <input type="text" name="phone" id="phone" autocomplete="tel" class="block w-full shadow-sm py-3 px-4 placeholder-gray-500 focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded-md" placeholder="Phone">
                </div>
                <div>
                    <label for="message" class="sr-only">Message</label>
                    <textarea id="message" name="message" rows="4" class="block w-full shadow-sm py-3 px-4 placeholder-gray-500 focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded-md" placeholder="Message"></textarea>
                </div>
                <div>
                    <button type="submit" class="inline-flex justify-center py-3 px-6 border border-transparent shadow-sm text-base font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        Submit
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
