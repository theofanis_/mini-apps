<div class="bg-white overflow-hidden shadow rounded-lg divide-y divide-gray-200" id="hours-in-period">

    <div class="px-4 py-5 sm:px-6">

            <h3 class="text-lg leading-6 font-medium text-gray-900">
                {{ __("Ώρες Απασχόλησης Εντός Περιόδου") }}
            </h3>
            <p class="mt-1 text-sm text-gray-500">
                Πόσες ώρες ο υπάλληλος εργάστηκε εντός της δοσμένης χρονικής περιόδου.
                Χρησιμεύει και για την εύρεση ωρών βραδινής απασχόλησης.
            </p>
    </div>
    <div class="px-4 py-5 sm:p-6">
        <form wire:submit.prevent="submit" class="space-y-2 divide-y divide-gray-200">

            <div class="grid grid-cols-2 gap-4">

                <div class="min-w-30 col-span-2 ">
                    <label for="hours-in-period.file" class="block text-sm font-medium text-gray-700">
                        {{ __('Spreadsheet') }}
                    </label>
                    <div class="mt-1 flex rounded-md shadow-sm">
                        <input type="file" wire:model="file" required id="hours-in-period.file"
                               class="w-full bg-white py-2 px-3 border border-gray-300 rounded-md shadow-sm text-sm  font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    </div>
                    @error('file')
                    <p class="mt-2 text-sm text-red-600">{{ $message }}</p>
                    @enderror
                </div>

                <div class="">
                    <label for="hours-in-period.period_begin" class="block text-sm font-medium text-gray-700">
                        {{ __('Period Begin') }}
                    </label>
                    <div class="mt-1 flex rounded-md shadow-sm">
                        <input type="text" wire:model.lazy="period_begin" required id="hours-in-period.period_begin" autocomplete="hours-in-period.period_begin"
                               class=" py-2 flex-1 focus:ring-indigo-500 focus:border-indigo-500 block w-1 min-w-0 rounded-none rounded-md sm:text-sm border-gray-300">
                    </div>
                    @error('period_begin')
                    <p class="mt-2 text-sm text-red-600">{{ $message }}</p>
                    @enderror
                </div>

                <div class="">
                    <label for="hours-in-period.period_end" class="block text-sm font-medium text-gray-700">
                        {{ __('Period End') }}
                    </label>
                    <div class="mt-1 flex rounded-md shadow-sm">
                        <input type="text" wire:model.lazy="period_end" required id="hours-in-period.period_end" autocomplete="hours-in-period.period_end"
                               class=" py-2 flex-1 focus:ring-indigo-500 focus:border-indigo-500 block w-1 min-w-0 rounded-none rounded-md sm:text-sm border-gray-300">
                    </div>
                    @error('period_end')
                    <p class="mt-2 text-sm text-red-600">{{ $message }}</p>
                    @enderror
                </div>

            </div>


            <div class="pt-2 flex flex-row justify-end">
                <div class="flex flex-grow">
                    <p class="text-sm text-gray-500 overflow-ellipsis">
                        Επεξεργάζεται μόνο το τελευταίο φύλλο. Η πρώτη γραμμή παραβλέπεται. Κάθε επόμενο κελί μπορεί να περιέχει ένα ζεύγος ωρών. Όσα πεδία δεν αναγνωρίζονται παραλείπονται. Στο εξαγόμενο χρωματίζονται με πράσινο τα επεξεργασμένο πεδία.
                        <a href="{{ asset('/demo/Παράδειγμα - Ώρες Εντός Περιόδου.xls') }}" download class="relative cursor-pointer bg-white rounded-md font-medium text-indigo-600 hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500">
                            Παράδειγμα αρχείου
                        </a>
{{--                        <span class="italic">Πεδία</span> είναι τα έγκυρα κελιά που αναγνωρίστηκαν. <span class="italic">Συνολικές Ώρες</span> είναι οι συνολικές ώρες που δούλεψε ο εργαζόμενος. <span class="italic">Ώρες Εντός Περιόδου</span> είναι οι ώρες που δούλεψε ο εργαζόμενος εντός της δοσμένης περιόδου.--}}
                    </p>
                </div>
                <div class="flex justify-end">
                    <button wire:loading.flex type="button" class="inline-flex items-center px-4 py-2 border border-transparent text-base leading-6 font-medium rounded-md text-white bg-gray-600 hover:bg-gray-500 focus:border-gray-700 active:bg-gray-700 transition ease-in-out duration-150 cursor-not-allowed" disabled="">
                        <svg class="animate-spin -ml-1 mr-3 h-5 w-5 text-white" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                            <circle class="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" stroke-width="4"></circle>
                            <path class="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path>
                        </svg>
                        {{ __('Processing') }}...
                    </button>

                    @if($validated)
                        <button wire:loading.remove type="submit" class="ml-3 inline-flex items-center justify-center py-2 px-4 border border-transparent leading-6  shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            <svg class="animate-bounce -ml-1 mr-3 h-5 w-5 text-white" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-4l-4 4m0 0l-4-4m4 4V4"/>
                            </svg>
                            {{ __('Download') }}
                        </button>
                    @endif
                </div>
            </div>
        </form>
    </div>
</div>
