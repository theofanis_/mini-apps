<div class="bg-white overflow-hidden shadow rounded-lg divide-y divide-gray-200" id="average-employees-annually">

    <div class="px-4 py-5 sm:px-6">

        <h3 class="text-lg leading-6 font-medium text-gray-900">
            {{ __("Average Employees Annually") }}
        </h3>
        <p class="mt-1 text-sm text-gray-500">
        </p>
    </div>
    <div class="px-4 py-5 sm:p-6 justify-content-end">
        <form wire:submit.prevent="submit" class="space-y-2 divide-y divide-gray-200">


            <div class="grid grid-cols-1  gap-4">

                <div class="min-w-30">
                    <label for="average-employees-annually.file" class="block text-sm font-medium text-gray-700">
                        {{ __('Spreadsheet') }}
                    </label>
                    <div class="mt-1 flex rounded-md shadow-sm">
                        <input type="file" wire:model="file" required id="average-employees-annually.file"
                               class="w-full bg-white py-2 px-3 border border-gray-300 rounded-md shadow-sm text-sm  font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    </div>
                    @error('file')
                    <p class="mt-2 text-sm text-red-600">{{ $message }}</p>
                    @enderror
                </div>

                <div class="">
                    <label for="average-employees-annually.year" class="block text-sm font-medium text-gray-700">
                        {{ __('Year') }}
                    </label>
                    <div class="mt-1 flex rounded-md shadow-sm">
                        <input type="number" wire:model.lazy="year" required id="average-employees-annually.year" autocomplete="average-employees-annually.year" min="0" step="1"
                               class=" py-2 flex-1 focus:ring-indigo-500 focus:border-indigo-500 block w-1 min-w-0 rounded-none rounded-md sm:text-sm border-gray-300">
                    </div>
                    @error('year')
                    <p class="mt-2 text-sm text-red-600">{{ $message }}</p>
                    @enderror
                </div>

            </div>

            <div class="pt-2 flex flex-row">
                <div class="flex flex-grow">
                    <p class="text-sm text-gray-500 overflow-ellipsis">
                        Απαραίτητες οι στήλες Όνομα, Ημ/νία Πρόσλ., Ημ/νία αποχ. χωρίς διάκριση πεζών, κεφαλαίων, τονισμών, σημείων στίξης και κομμένων ή ολόκληρων λέξεων.
                        <a href="{{ asset('/demo/Παράδειγμα - ΜΟ Εργαζόμενων Ετησίως.xls') }}" download class="relative cursor-pointer bg-white rounded-md font-medium text-indigo-600 hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500">
                            Παράδειγμα αρχείου
                        </a>
                    </p>
                </div>
                <div class="flex justify-end">
                    <button wire:loading.flex type="button" class="inline-flex items-center px-4 py-2 border border-transparent text-base leading-6 font-medium rounded-md text-white bg-gray-600 hover:bg-gray-500 focus:border-gray-700 active:bg-gray-700 transition ease-in-out duration-150 cursor-not-allowed" disabled="">
                        <svg class="animate-spin -ml-1 mr-3 h-5 w-5 text-white" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                            <circle class="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" stroke-width="4"></circle>
                            <path class="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path>
                        </svg>
                        {{ __('Processing') }}...
                    </button>

                    @if($validated)
                        <button wire:loading.remove type="submit" class="ml-3 inline-flex items-center justify-center py-2 px-4 border border-transparent leading-6  shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            <svg class="animate-bounce -ml-1 mr-3 h-5 w-5 text-white" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-4l-4 4m0 0l-4-4m4 4V4"/>
                            </svg>
                            {{ __('Download') }}
                        </button>
                    @endif
                </div>
            </div>
        </form>
    </div>
</div>
