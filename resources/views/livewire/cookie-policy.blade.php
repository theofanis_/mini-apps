@if ($showBanner)
<div class=" bottom-0 inset-x-0 pb-2 sm:pb-5"
     x-data="{ showBanner: $wire.showBanner }"
     x-show="showBanner"
     x-transition:enter=" delay-100 transition ease-out duration-200"
     x-transition:enter-start="transform opacity-0 scale-95"
     x-transition:enter-end="transform opacity-100 scale-100"
     x-transition:leave="transition ease-in duration-95"
     x-transition:leave-start="transform opacity-100 scale-100 "
     x-transition:leave-end="transform opacity-0 scale-90 translate-y-12"
>
    <div class="max-w-7xl mx-auto px-2 sm:px-6 lg:px-8">
        <div class="p-2 rounded-lg bg-indigo-600 shadow-lg sm:p-3">
            <div class="flex items-center justify-between flex-wrap">
                <div class="w-0 flex-1 flex items-center">
                    <span class="flex p-2 rounded-lg bg-indigo-800">
                        <svg class="h-3 w-3 sm:h-6 sm:w-6 text-white" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"/>
                        </svg>
                    </span>
                    <p class="ml-2 font-medium text-white">
                        <span class="md:hidden">
                            {{ __('Cookie Policy') }}
                        </span>
                        <span class="hidden md:inline">
                            {{ __("This website uses cookies to improve your experience. We'll assume you're ok with this, but you can opt-out if you wish.") }}
                        </span>
                    </p>
                </div>
                <button type="button" wire:click="accept" @click="showBanner = false" class="flex items-center justify-center px-2 py-1 sm:px-4 sm:py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-indigo-600 bg-white hover:bg-indigo-50">
                    {{ __('Accept') }}
                </button>
                <a href="{{ route('policy.show') }}" class="-mr-1 flex p-2 rounded-md hover:bg-indigo-500 focus:outline-none focus:ring-2 focus:ring-white text-white sm:ml-2">
                    {{ __('More') }}
                </a>
            </div>
        </div>
    </div>
</div>
@endif
