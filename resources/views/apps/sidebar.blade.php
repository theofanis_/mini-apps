{{-- Off-canvas menu for mobile, show/hide based on off-canvas menu state. --}}
<div class="fixed inset-0 flex z-40 md:hidden" role="dialog" aria-modal="true"
     x-show="isSidebarOpen"
     :aria-hidden="!isSidebarOpen"
     :aria-expanded="isSidebarOpen"
>
    {{--
      Off-canvas menu overlay, show/hide based on off-canvas menu state.

      Entering: "transition-opacity ease-linear duration-300"
        From: "opacity-0"
        To: "opacity-100"
      Leaving: "transition-opacity ease-linear duration-300"
        From: "opacity-100"
        To: "opacity-0"
    --}}
    <div class="fixed inset-0 bg-gray-600 bg-opacity-75"
         x-show="isSidebarOpen"
         x-transition:enter="transition-opacity ease-linear duration-300"
         x-transition:enter-start="opacity-0"
         x-transition:enter-end="opacity-100"
         x-transition:leave="transition-opacity ease-linear duration-300"
         x-transition:leave-start="opacity-100"
         x-transition:leave-end="opacity-0"
    ></div>

    {{--
      Off-canvas menu, show/hide based on off-canvas menu state.

      Entering: "transition ease-in-out duration-300 transform"
        From: "-translate-x-full"
        To: "translate-x-0"
      Leaving: "transition ease-in-out duration-300 transform"
        From: "translate-x-0"
        To: "-translate-x-full"
    --}}
    <div class="relative flex-1 flex flex-col max-w-xs w-full pt-5 pb-4 bg-indigo-700"
         x-show="isSidebarOpen"
         x-transition:enter="transition ease-in-out duration-300 transform"
         x-transition:enter-start="-translate-x-full"
         x-transition:enter-end="translate-x-0"
         x-transition:leave="transition ease-in-out duration-300 transform"
         x-transition:leave-start="translate-x-0"
         x-transition:leave-end="-translate-x-full"
    >
        {{--
          Close button, show/hide based on off-canvas menu state.

          Entering: "ease-in-out duration-300"
            From: "opacity-0"
            To: "opacity-100"
          Leaving: "ease-in-out duration-300"
            From: "opacity-100"
            To: "opacity-0"
        --}}
        <div class="absolute top-0 right-0 -mr-12 pt-2"
             x-show="isSidebarOpen"
             x-transition:enter="ease-in-out duration-300"
             x-transition:enter-start="opacity-0"
             x-transition:enter-end="opacity-100"
             x-transition:leave="ease-in-out duration-300"
             x-transition:leave-start="opacity-100"
             x-transition:leave-end="opacity-0"
        >
            <button type="button" class="ml-1 flex items-center justify-center h-10 w-10 rounded-full focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white"
                    @click="isSidebarOpen = false"
                    x-show="isSidebarOpen"
            >
                <span class="sr-only">Close sidebar</span>
                <svg class="h-6 w-6 text-white" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"/>
                </svg>
            </button>
        </div>

        <div class="flex-shrink-0 flex items-center px-4">
            <img class="h-8 w-auto" src="https://tailwindui.com/img/logos/workflow-logo-indigo-300-mark-white-text.svg" alt="Workflow">
        </div>
        <div class="mt-5 flex-1 flex flex-col overflow-y-auto">
            <nav class="flex-1 px-2 space-y-1">
                <x-jet-apps-category-link href="{{ route('apps.accounting') }}" :active="request()->routeIs('apps.accounting')" :mobile="true">
                    <svg class="mr-4 h-6 w-6 text-indigo-300" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 7h6m0 10v-3m-3 3h.01M9 17h.01M9 14h.01M12 14h.01M15 11h.01M12 11h.01M9 11h.01M7 21h10a2 2 0 002-2V5a2 2 0 00-2-2H7a2 2 0 00-2 2v14a2 2 0 002 2z"/>
                    </svg>
                    {{ __('Accounting') }}
                </x-jet-apps-category-link>

                <x-jet-apps-category-link href="{{ route('apps.secretary') }}" :active="request()->routeIs('apps.secretary')" :mobile="true">
                    <svg class="mr-4 h-6 w-6 text-indigo-300" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 8h14M5 8a2 2 0 110-4h14a2 2 0 110 4M5 8v10a2 2 0 002 2h10a2 2 0 002-2V8m-9 4h4"/>
                    </svg>
                    {{ __('Secretary') }}
                </x-jet-apps-category-link>

{{--                <x-jet-apps-category-link href="{{ route('apps.sales') }}" :active="request()->routeIs('apps.sales')" :mobile="true">--}}
{{--                    <svg class="mr-4 h-6 w-6 text-indigo-300" fill="none" viewBox="0 0 24 24" stroke="currentColor">--}}
{{--                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M7 12l3-3 3 3 4-4M8 21l4-4 4 4M3 4h18M4 4h16v12a1 1 0 01-1 1H5a1 1 0 01-1-1V4z"/>--}}
{{--                    </svg>--}}
{{--                    {{ __('Sales') }}--}}
{{--                </x-jet-apps-category-link>--}}

                <x-jet-apps-category-link href="{{ route('apps.multimedia') }}" :active="request()->routeIs('apps.multimedia')" :mobile="true">
                    <svg class="mr-4 h-6 w-6 text-indigo-300" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 10l4.553-2.276A1 1 0 0121 8.618v6.764a1 1 0 01-1.447.894L15 14M5 18h8a2 2 0 002-2V8a2 2 0 00-2-2H5a2 2 0 00-2 2v8a2 2 0 002 2z"></path>
                    </svg>
                    {{ __('Multimedia') }}
                </x-jet-apps-category-link>
            </nav>

            <div class="space-y-1">
                <h3 class="px-3 text-xs font-semibold text-gray-500 uppercase tracking-wider ">
                    <br>
                </h3>
                <div class="space-y-1" role="group">

                    <a href="mailto:vardtheo@theograms.tech" class="group flex items-center px-3 py-2 text-sm font-medium text-indigo-200 rounded-md hover:text-white">
                        <span class="truncate">
                            {{ __('Contact') }}
                        </span>
                    </a>

                    <a href="{{ route('policy.show') }}" class="group flex items-center px-3 py-2 text-sm font-medium text-indigo-200 rounded-md hover:text-white">
                        <span class="truncate">
                            {{ __('Privacy Policy') }}
                        </span>
                    </a>

                    {{--                    <a href="{{ route('terms.show') }}" class="group flex items-center px-3 py-2 text-sm font-medium text-indigo-200 rounded-md hover:text-white">--}}
                    {{--                        <span class="truncate">--}}
                    {{--                            {{ __('Terms of Service') }}--}}
                    {{--                        </span>--}}
                    {{--                    </a>--}}

                </div>
            </div>
        </div>
    </div>

    <div class="flex-shrink-0 w-14" aria-hidden="true">
        {{-- Dummy element to force sidebar to shrink to fit close icon --}}
    </div>
</div>

{{-- Static sidebar for desktop --}}
<div class="hidden bg-indigo-700 md:flex md:flex-shrink-0">
    <div class="flex flex-col w-64">

        <div class="flex flex-col flex-grow pt-5 pb-4 overflow-y-auto">
            <div class="flex items-center flex-shrink-0 px-4">
                <img class="h-8 w-auto" src="https://tailwindui.com/img/logos/workflow-logo-indigo-300-mark-white-text.svg" alt="Workflow">
            </div>
            <div class="mt-5 flex-1 flex flex-col">
                <nav class="flex-1 px-2 space-y-1">
                    <x-jet-apps-category-link href="{{ route('apps.accounting') }}" :active="request()->routeIs('apps.accounting')">
                        <svg class="mr-4 h-6 w-6 text-indigo-300" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 7h6m0 10v-3m-3 3h.01M9 17h.01M9 14h.01M12 14h.01M15 11h.01M12 11h.01M9 11h.01M7 21h10a2 2 0 002-2V5a2 2 0 00-2-2H7a2 2 0 00-2 2v14a2 2 0 002 2z"/>
                        </svg>
                        {{ __('Accounting') }}
                    </x-jet-apps-category-link>

                    <x-jet-apps-category-link href="{{ route('apps.secretary') }}" :active="request()->routeIs('apps.secretary')">
                        <svg class="mr-4 h-6 w-6 text-indigo-300" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 8h14M5 8a2 2 0 110-4h14a2 2 0 110 4M5 8v10a2 2 0 002 2h10a2 2 0 002-2V8m-9 4h4"/>
                        </svg>
                        {{ __('Secretary') }}
                    </x-jet-apps-category-link>

{{--                    <x-jet-apps-category-link href="{{ route('apps.sales') }}" :active="request()->routeIs('apps.sales')">--}}
{{--                        <svg class="mr-4 h-6 w-6 text-indigo-300" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">--}}
{{--                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M7 12l3-3 3 3 4-4M8 21l4-4 4 4M3 4h18M4 4h16v12a1 1 0 01-1 1H5a1 1 0 01-1-1V4z"/>--}}
{{--                        </svg>--}}
{{--                        {{ __('Sales') }}--}}
{{--                    </x-jet-apps-category-link>--}}

                    <x-jet-apps-category-link href="{{ route('apps.multimedia') }}" :active="request()->routeIs('apps.multimedia')" :mobile="true">
                        <svg class="mr-4 h-6 w-6 text-indigo-300" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 10l4.553-2.276A1 1 0 0121 8.618v6.764a1 1 0 01-1.447.894L15 14M5 18h8a2 2 0 002-2V8a2 2 0 00-2-2H5a2 2 0 00-2 2v8a2 2 0 002 2z"></path>
                        </svg>
                        {{ __('Multimedia') }}
                    </x-jet-apps-category-link>
                </nav>

                <div class="space-y-1">
                    <h3 class="px-3 text-xs font-semibold text-gray-500 uppercase tracking-wider ">
                        <br>
                    </h3>
                    <div class="space-y-1" role="group">

                        <a href="mailto:vardtheo@gmail.com" class="group flex items-center px-3 py-2 text-sm font-medium text-indigo-200 rounded-md hover:text-white">
                            <span class="truncate">
                                {{ __('Contact') }}
                            </span>
                        </a>

                        <a href="{{ route('policy.show') }}" class="group flex items-center px-3 py-2 text-sm font-medium text-indigo-200 rounded-md hover:text-white">
                            <span class="truncate">
                                {{ __('Privacy Policy') }}
                            </span>
                        </a>

                        {{--                        <a href="{{ route('terms.show') }}" class="group flex items-center px-3 py-2 text-sm font-medium text-indigo-200 rounded-md hover:text-white">--}}
                        {{--                            <span class="truncate">--}}
                        {{--                                {{ __('Terms of Service') }}--}}
                        {{--                            </span>--}}
                        {{--                        </a>--}}

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>




