<x-apps-layout>

    <main class="flex-1 relative overflow-y-auto focus:outline-none" tabindex="0">
        <div class="py-3">
            <div class=" mx-auto px-4 sm:px-6 md:px-8">
                <h1 class="text-2xl font-semibold text-gray-900 pb-3">{{ __('Sales') }}</h1>
            </div>
            <div class=" mx-auto px-4 sm:px-6 md:px-8">
                ...
            </div>
            @include('apps.footer-new-function')
        </div>
    </main>

</x-apps-layout>
