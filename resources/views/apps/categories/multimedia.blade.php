<x-apps-layout>

    <main class="flex-1 relative overflow-y-auto focus:outline-none" tabindex="0">
        <div class="py-3">
            <div class=" mx-auto px-4 sm:px-6 md:px-8">
                <h1 class="text-2xl font-semibold text-gray-900 pb-3">Multimedia</h1>
            </div>
            <div class="grid grid-cols-1 md:grid-cols-2 mx-auto px-4 sm:px-6 md:px-8 gap-4 md:gap-12">
                @livewire('apps.youtube-download')
            </div>
            @include('apps.footer-new-function')
        </div>
    </main>

</x-apps-layout>
