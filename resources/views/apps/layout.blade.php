<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    {{-- Fonts --}}
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

    {{-- Styles --}}
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    @livewireStyles

    {{-- Scripts --}}
    <script src="{{ mix('js/app.js') }}" defer></script>
</head>
<body class="font-sans antialiased">
    <div class="h-screen flex overflow-hidden bg-gray-100 main-pattern"
         x-data="{ isSidebarOpen: false }"
    >
        @include('apps.sidebar')

{{--        <svg class="absolute h-screen w-screen top-0 left-0"  fill="none" viewBox="0 0 500 500">--}}
{{--            <defs>--}}
{{--                <pattern id="837c3e70-6c3a-44e6-8854-cc48c737b659" x="0" y="0" width="20" height="20" patternUnits="userSpaceOnUse">--}}
{{--                    <rect x="0" y="0" width="4" height="4" class="text-gray-200" fill="currentColor"></rect>--}}
{{--                </pattern>--}}
{{--            </defs>--}}
{{--            <rect class="h-full w-full" width="1000" height="1000" fill="url(#837c3e70-6c3a-44e6-8854-cc48c737b659)"></rect>--}}
{{--        </svg>--}}

        <div class="flex flex-col w-0 flex-1 overflow-y-auto">
            @include('apps.topbar')

            {{-- Page Content --}}
            {{ $slot }}

            @livewire('cookie-policy')
        </div>

    </div>

    @stack('modals')

    @livewireScripts
</body>
</html>
