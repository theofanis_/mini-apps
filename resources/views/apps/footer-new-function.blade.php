 <div class="max-w-7xl mx-auto pt-4 px-4 sm:px-6 md:flex md:items-center md:justify-between lg:px-8">
        <div class="flex flex-1 justify-center space-x-6 md:order-2">
            <a href="/contact?reason=new-app" class="text-gray-400 hover:text-gray-600 hover:underline">
                {{ __("Didn't find what you are looking for?") }}
                <br class="sm:hidden">
                {{ __('Request new function.') }}
            </a>
    </div>
 </div>

