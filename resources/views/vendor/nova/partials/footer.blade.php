<p class="mt-8 text-center text-xs text-80">
    <a href="https://www.linkedin.com/in/theofanis-vardatsikos/" class="text-primary dim no-underline">Mini Apps</a>
    <span class="px-1">&middot;</span>
    &copy; {{ date('Y') }} Theograms.tech - By Theofanis Vardatsikos.
</p>
