<x-apps-layout>
    <div class="pt-4 bg-gray-100 overflow-y-auto">
        <div class="min-h-screen flex flex-col items-center pt-6 sm:pt-0">
            <div class="w-full sm:max-w-2xl mt-6 p-6 bg-white shadow-md sm:rounded-lg prose">
                {!! $policy !!}
            </div>
        </div>
    </div>
</x-apps-layout>
