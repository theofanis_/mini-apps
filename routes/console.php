<?php

use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('clear:caches', function () {
    $this->call('clear-compiled');
    $this->call('cache:clear');
    $this->call('config:clear');
    $this->call('route:clear');
    $this->call('view:clear');
})->describe('Clear all application caches');

Artisan::command('ide-helper:all', function () {
    rescue(function () {
        $this->call('ide-helper:eloquent');
        $this->call('ide-helper:model', ['--reset' => true, '--write' => true]);
        $this->call('ide-helper:generate');
        $this->call('ide-helper:meta');
    });
})->describe('Call all ide-helper functions.');

Artisan::command('db:seed:permissions', function () {
    $this->call('db:seed', ['--class' => 'DefaultPermissionsSeeder', '--force' => true]);
    $this->call('cache:clear');
    $this->call('optimize');
})->describe('ReSeed permissions');

Artisan::command('db:seed:roles-permissions', function () {
    $this->call('db:seed', ['--class' => 'DefaultRolesSeeder', '--force' => true]);
    $this->call('db:seed', ['--class' => 'DefaultPermissionsSeeder', '--force' => true]);
    $this->call('db:seed', ['--class' => 'DefaultRolesWithPermissionsSeeder', '--force' => true]);
    $this->call('cache:clear');
    $this->call('optimize');
})->describe('ReSeed permissions, roles and their associations');

Artisan::command('seed:apps', function () {
    $this->call('db:seed', ['--class' => \Database\Seeders\MiniAppsSeeder::class, '--force' => true]);
    $this->table(['ID', 'Code', 'Component', 'Created At'], app('mini-apps')->map(function ($component, $code) {
        $app = \App\Models\MiniApp::findByCode($code);
        return [$app->id, $app->code, $app->component, $app->created_at];
    }));
})->describe('Register apps to database.');

Artisan::command('try', function () {
    $link = 'https://www.youtube.com/watch?v=dQw4w9WgXcQ';
    $youtube = new YouTube\YouTubeDownloader();
    $downloadOptions = $youtube->getDownloadLinks($link);
//    dump($downloadOptions->getAudioFormats());
//    dump($downloadOptions->getVideoFormats());
//    dump($downloadOptions->getCombinedFormats());
//    dump($downloadOptions->getFirstCombinedFormat()->getCleanMimeType());
//    dump($downloadOptions->getInfo());
    dump(collect($downloadOptions->getAllFormats())->toArray());
});
