<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/apps/accounting');
Route::redirect('/apps', '/apps/accounting')->name('apps');

Route::view('/contact', 'contact')->name('contact');

Route::prefix('/apps')->name('apps.')->group(function () {
    Route::view('accounting', 'apps.categories.accounting')->name('accounting');
    Route::view('secretary', 'apps.categories.secretary')->name('secretary');
    Route::view('sales', 'apps.categories.sales')->name('sales');
    Route::view('multimedia', 'apps.categories.multimedia')->name('multimedia');
});

Route::view('/dashboard', 'dashboard')->middleware(['auth'])->name('dashboard');

