Mini Apps
=========


# Installation

```shell
sudo apt install -y python3-pip
sudo apt install -y ffmpeg
sudo pip3 install youtube-dl

php artisan migrate --seed --force
php artisan storage:link
php artisan setup:path
```

```nginx configuration
fastcgi_read_timeout 600s;
```
