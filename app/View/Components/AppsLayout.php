<?php

namespace App\View\Components;

use Illuminate\View\Component;

class AppsLayout extends Component
{

    public function render()
    {
        return view('apps.layout');
    }
}
