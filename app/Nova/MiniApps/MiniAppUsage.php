<?php

namespace App\Nova\MiniApps;

use App\Nova\Resource;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\KeyValue;
use Laravel\Nova\Fields\Text;

class MiniAppUsage extends Resource
{
    public static $model = \App\Models\MiniAppUsage::class;
    public static $title = 'mini_app_code';
    public static $globallySearchable = false;
    public static $search = [
        'id', 'mini_app_code',
    ];

    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make(__('Mini App Code'), 'mini_app_code'),
            BelongsTo::make(__('Mini App'), 'app', \App\Nova\MiniApps\MiniApp::class),
            Text::make(__('Session'), 'session'),
            BelongsTo::make(__('User'), 'user', \App\Nova\Management\User::class),
            KeyValue::make(__('Info'), 'info'),
        ];
    }

    public function actions(Request $request)
    {
        return [
            $this->actionDownloadExcel(),
        ];
    }
}
