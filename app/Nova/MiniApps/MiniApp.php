<?php

namespace App\Nova\MiniApps;

use App\Nova\Resource;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;

class MiniApp extends Resource
{
    public static $model = \App\Models\MiniApp::class;
    public static $title = 'name';
    public static $subtitle = 'code';
    public static $search = [
        'id', 'code',
    ];

    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make(__('Name'), 'name')->readonly(),
            Text::make(__('code'), 'code')->readonly(),
            HasMany::make(__('Usages'), 'usages', \App\Nova\MiniApps\MiniAppUsage::class),
            $this->timestampablePanel($request),
        ];
    }

    public function actions(Request $request)
    {
        return [
            $this->actionDownloadExcel(),
        ];
    }
}
