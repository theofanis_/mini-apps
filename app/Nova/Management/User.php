<?php

namespace App\Nova\Management;

use App\Nova\Actions\CopyPermissionsFromRole;
use App\Nova\Actions\GenerateApiToken;
use App\Nova\Resource;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Gravatar;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\MorphToMany;
use Laravel\Nova\Fields\Password;
use Laravel\Nova\Fields\Text;

class User extends Resource
{
    public static $model = \App\Models\User::class;

    public static $title = 'name';

    public static $search = [
        'id', 'name', 'email', 'username', 'phone',
    ];

    public static $group = 'Management';

    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Gravatar::make(),
            Text::make(__('Name'), 'name')
                ->sortable()
                ->rules('required', 'max:255'),
            Text::make(__('Email'), 'email')
                ->sortable()
                ->rules(['email', 'max:60'])
                ->creationRules(['unique:users,email'])
                ->updateRules(['unique:users,email,{{resourceId}}']),
            Text::make(__('Username'), 'username')
                ->sortable()
                ->rules('nullable', 'max:254')
                ->creationRules('unique:users,username')
                ->updateRules('unique:users,username,{{resourceId}}'),
            Password::make(__('Password'), 'password')
                ->onlyOnForms()
                ->creationRules('required', 'string', 'min:8')
                ->updateRules('nullable', 'string', 'min:8'),
            MorphToMany::make(__('Roles'), 'roles', \App\Nova\Management\Role::class),
            MorphToMany::make(__('Permissions'), 'permissions', \App\Nova\Management\Permission::class),
            $this->groupedBlameablePanel($request),
        ];
    }

    public function actions(Request $request)
    {
        return [
            $this->actionDownloadExcel(),
            new CopyPermissionsFromRole(),
        ];
    }

    public function subtitle()
    {
        return $this->email;
    }

}
