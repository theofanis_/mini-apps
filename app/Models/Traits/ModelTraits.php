<?php
/**
 * ModelTraits.php
 * User: Theofanis V. (vardtheo@gmail.com)
 * Date: 29/9/20 1:53 PM
 */

namespace App\Models\Traits;


use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use RichanFongdasen\EloquentBlameable\BlameableService;
use RichanFongdasen\EloquentBlameable\BlameableTrait;

trait ModelTraits
{
    use SoftDeletes;
    use BlameableTrait;

    public function deleter(): BelongsTo
    {
        return $this->belongsTo(
            app(BlameableService::class)->getConfiguration($this, 'user'),
            app(BlameableService::class)->getConfiguration($this, 'deletedBy')
        );
    }

}
