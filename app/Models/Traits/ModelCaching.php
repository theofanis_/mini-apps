<?php

namespace App\Models\Traits;


use Closure;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;

trait ModelCaching
{

    protected static function bootModelCaching()
    {
        static::saved(function () {
            Cache::tags(static::class)->flush();
        });
        static::deleted(function () {
            Cache::tags(static::class)->flush();
        });
        if (in_array(SoftDeletes::class, class_uses_recursive(static::class), true)) {
            static::restored(function () {
                Cache::tags(static::class)->flush();
            });
        }
    }

    /**
     * @param string  $key
     * @param Closure $callback
     * @return static|static[]|\Illuminate\Database\Eloquent\Collection
     */
    protected static function cache(string $key, Closure $callback)
    {
        return Cache::tags(static::class)->remember($key, config('cache.duration', 604800), $callback);
    }

}
