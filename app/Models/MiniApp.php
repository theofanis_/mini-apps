<?php

namespace App\Models;

use App\Exceptions\AppUsageException;
use App\Http\Livewire\Apps\AppComponent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

/**
 * App\Models\MiniApp
 *
 * @property int $id
 * @property string $code
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read string|\App\Http\Livewire\Apps\AppComponent $component
 * @property-read string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\MiniAppUsage[] $usages
 * @property-read int|null $usages_count
 * @method static \Illuminate\Database\Eloquent\Builder|MiniApp newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MiniApp newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MiniApp query()
 * @method static \Illuminate\Database\Eloquent\Builder|MiniApp whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MiniApp whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MiniApp whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MiniApp whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class MiniApp extends Model
{
    protected $fillable = ['code'];

    /**
     * @param string $code
     * @return static
     */
    public static function findByCode($code)
    {
        return static::whereCode($code)->firstOrFail();
    }

    /**
     * @return string
     */
    public function getNameAttribute()
    {
        return humanize_attr($this->code);
    }

    /**
     * @return MiniAppUsage|null
     */
    public function recordUsage($info = [])
    {
        try {
            return $this->usages()->create($attrs = ['user_id' => Auth::id(), 'session' => Session::getId(), 'mini_app_code' => $this->code, 'info' => $info]);
        } catch (\Throwable $e) {
            report((new AppUsageException("Failed to record usage: {$this->code}", 0, $e))->withContext(['MiniApp' => $this, 'MiniAppUsage' => $attrs]));
        }
        return null;
    }

    /**
     * @return string|AppComponent
     */
    public function getComponentAttribute()
    {
        return app('mini-apps')[$this->code];
    }

    public function usages()
    {
        return $this->hasMany(MiniAppUsage::class);
    }

}
