<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\MiniAppUsage
 *
 * @property int $id
 * @property int $mini_app_id
 * @property string $mini_app_code
 * @property string|null $session
 * @property int|null $user_id
 * @property array $info
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\MiniApp $app
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|MiniAppUsage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MiniAppUsage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MiniAppUsage query()
 * @method static \Illuminate\Database\Eloquent\Builder|MiniAppUsage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MiniAppUsage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MiniAppUsage whereInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MiniAppUsage whereMiniAppCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MiniAppUsage whereMiniAppId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MiniAppUsage whereSession($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MiniAppUsage whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MiniAppUsage whereUserId($value)
 * @mixin \Eloquent
 */
class MiniAppUsage extends Model
{
    protected $fillable = ['mini_app_id', 'mini_app_code', 'user_id', 'session', 'info',];
    protected $casts = [
        'info' => 'array',
    ];
    protected $attributes = [
        'info' => '{}',
    ];

    protected static function booted()
    {
        static::creating(function (MiniAppUsage $usage) {
            if (empty($usage->mini_app_code)) {
                $usage->mini_app_code = $usage->app->code;
            }
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function app()
    {
        return $this->belongsTo(MiniApp::class, 'mini_app_id');
    }

}
