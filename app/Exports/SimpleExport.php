<?php

namespace App\Exports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class SimpleExport implements FromArray, WithTitle, WithStyles, ShouldAutoSize, WithHeadings
{
    use Exportable;

    /**
     * @var array[]
     */
    public $data;
    /**
     * @var string
     */
    public $title;
    /**
     * @var string[]
     */
    public $headings = [];
    public $bold_rows_cols = [];

    public static function make($data = [], $title = '')
    {
        return new static($data, $title);
    }

    public function __construct($data = [], $title = '')
    {
        $this->data = $data instanceof Collection ? $data->all() : $data;
        $this->title = $title;
    }

    /**
     * @param array $data
     * @return $this
     */
    public function withData($data)
    {
        $this->data = $data instanceof Collection ? $data->all() : $data;
        return $this;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function withTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @param string[] $headings
     * @return $this
     */
    public function withHeadings($headings)
    {
        $this->headings = $headings;
        return $this;
    }

    /**
     * @param mixed $cells Include rows, or columns
     * @return $this
     */
    public function withBold(...$cells)
    {
        $this->bold_rows_cols = func_get_args();
        return $this;
    }

    public function array(): array
    {
        return $this->data;
    }

    public function styles(Worksheet $sheet)
    {
        if (is_array($a = head($this->bold_rows_cols)))
            return $a;
        foreach ($this->bold_rows_cols as $row_col) {
            $sheet->getStyle($row_col)->getFont()->setBold(true);
        }
    }

    public function title(): string
    {
        return $this->title;
    }

    public function headings(): array
    {
        return $this->headings;
    }
}
