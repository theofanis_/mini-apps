<?php

namespace App\Exports;

use Illuminate\Support\Carbon;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ExportHoursInPeriod implements FromArray, WithTitle, WithStyles, ShouldAutoSize
{
    use Exportable;
    use ExportUtils;

    /**
     * @var Carbon
     */
    public $begin;
    /**
     * @var Carbon
     */
    public $end;
    /**
     * @var array|\SplFileInfo
     */
    public $data;
    /**
     * @var int
     */
    public $max_cells;
    public $cells_processed = [];

    /**
     * @param \SplFileInfo $data
     * @param string $begin
     * @param string $end
     */
    public function __construct($data, $begin, $end)
    {
        $this->data = $data;
        $this->begin = $this->time($begin);
        $this->end = $this->time($end);
        // in case start time is at the end of the day and ends to the next
        if ($this->end->isBefore($this->begin)) {
            $this->end->addDay();
        }
    }

    public function map($row, $row_index): array
    {
        // if there is no name then omit processing
        if (empty($row[0])) {
            return $row;
        }

        $data = [];
        $data['hours_in_period'] = 0;
        $data['hours_total'] = 0;
        $data['fields'] = 0;
        foreach ($row as $index => $field) {
            $data[$index] = $field;

            // normalize cell data
            $datum = str_replace(' ', '', $field);
            // find text in cell matching time, e.g. 1:00, 23.59
            preg_match_all('/(?<time>([01]?[0-9]|2[0-3])[:.]?([0-5][0-9]))/', $datum, $matches);
            $matches = $matches['time'];
            // if no valid time period is found leave current cell and continue
            // for valid time period there should be at least two matches of time, one for begin and one for end
            if (count($matches) < 2) {
                continue;
            }

            // increment number of valid fields processed
            $data['fields']++;
            $this->cells_processed[] = [$row_index + 1, $index + 1];

            $begin = $this->time(str_replace('.', ':', $matches[0]));
            $end = $this->time(str_replace('.', ':', $matches[1]));
            // in case start time is at the end of the day and ends to the next
            if ($end->isBefore($begin)) {
                $end->addDay();
            }

            // save the total hours for extra statistic
            $data['hours_total'] += $hours_total = round($begin->floatDiffInRealHours($end), 2);

            $effective_begin = max($this->begin, $begin);
            $effective_end = min($this->end, $end);
            $hours_in_period = round($effective_begin->floatDiffInRealHours($effective_end, false), 2);
            // in case begin is after end this gives negative number so exclude it
            $data['hours_in_period'] += $hours_in_period = max($hours_in_period, 0);

            $hours_in_period2 = 0;
            if (!$begin->isSameDay($end) || !$this->begin->isSameDay($this->end)) {
                $effective_begin = max($this->begin->clone()->subDay(), $begin);
                $effective_end = min($this->end->clone()->subDay(), $end);
                $hours_in_period2 = round($effective_begin->floatDiffInRealHours($effective_end, false), 2);
                // in case begin is after end this gives negative number so exclude it
                $data['hours_in_period'] += $hours_in_period2 = max($hours_in_period2, 0);
            }
            $total_hours_in_period = $hours_in_period + $hours_in_period2;

            // save the formatted cell data
            $data[$index] = "{$begin->format('H:i')} - {$end->format('H:i')} ($hours_total, $total_hours_in_period)";
        }
        return $data;
    }

    /**
     * @param null|string $time
     * @return Carbon
     */
    private function time($time = null)
    {
        $d = new Carbon('2021-04-01 00:00:00');
        return $time ? $d->setTimeFrom($time) : $d;
    }

    protected function footer($heading)
    {
        $footer = array_fill_keys(array_keys($heading), null);
        return [
                0                 => __('Sum'),
                'fields'          => array_sum(array_column($this->data, 'fields')),
                'hours_total'     => array_sum(array_column($this->data, 'hours_total')),
                'hours_in_period' => array_sum(array_column($this->data, 'hours_in_period')),
            ] + $footer;
    }

    protected function heading(&$data)
    {
        $heading = array_pull($data, 0);
        foreach ($heading as $index => $cell) {
            // may be dates so try parse them and print them
            if (empty($cell))
                continue;
            try {
                $heading[$index] = Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($cell))->toDateString();
            } catch (\Exception $e) {
            }
        }
        $heading['fields'] = __('Valid Fields');
        $heading['hours_total'] = __('Total Hours');
        $heading['hours_in_period'] = __('Hours In Period');
        return $heading;
    }

    public function array(): array
    {
        $data = $this->spreadsheetToArray($this->data);
        // pick only first sheet
        $data = last($data);

        $this->max_cells = max(array_map('count', $data));

        $heading = $this->heading($data);
        $this->data = [$heading];

        foreach ($data as $index => $row) {
            $this->data[] = $this->map($row, $index);
        }

        $this->data[] = $f = $this->footer($heading);

        $this->data = collect($this->data)->filter()->values();
        $this->data = $this->data->map(function ($row) {
            return collect($row)->keyBy(function ($value, $key) {
                // switch cannot handle int 0
                // https://stackoverflow.com/questions/19901340/how-to-use-switch-with-integer-0-in-php
                switch ((string)$key) {
                    case 'fields':
                        return $this->max_cells;
                    case 'hours_total':
                        return $this->max_cells + 1;
                    case 'hours_in_period':
                        return $this->max_cells + 2;
                    default:
                        return $key;
                }
            })->sortKeys()->all();
        });

        return $this->data->all();
    }

    public function title(): string
    {
        return __("Hours In Period :period", ['period' => "{$this->begin->format('H:i')}-{$this->end->format('H:i')}"]);
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('A')->getFont()->setBold(true);
        $sheet->getStyle(1)->getFont()->setBold(true);
        $sheet->getStyle($sheet->getHighestRow())->getFont()->setBold(true);
        // style background
        // https://laracasts.com/discuss/channels/laravel/how-to-style-background-in-empty-cells-in-excel
        foreach ($this->cells_processed as $cell) {
            $sheet->getCellByColumnAndRow($cell[1], $cell[0])->getStyle()->getFill()->setFillType(Fill::FILL_SOLID)->setStartColor(new Color(Color::COLOR_GREEN));
        }
    }
}
