<?php
/**
 * ExportUtils.php
 * User: Theofanis V. (vardtheo@gmail.com)
 * Date: 29/3/21 12:28 π.μ.
 */

namespace App\Exports;


use App\Imports\ImportSpreadsheetToData;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Importer;

/**
 * @property array $map_keys
 */
trait ExportUtils
{
    /**
     * @param \SplFileInfo|string $file
     * @return array
     */
    protected function spreadsheetToArray($file)
    {
        return Excel::toArray(app(Importer::class), $file);
    }

    protected function parseDate($value): ?Carbon
    {
        if (blank($value))
            return null;

        if (is_numeric($value) && $value < 70000) {
            $value = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value);
        }else {
            $value = str_replace('/', '-', $value);
        }

        return new Carbon($value);
    }

    public function title(): string
    {
        return humanize(str_after(class_basename(static::class), 'Export'));
    }

    /**
     * @param array|Collection|\SplFileInfo $data
     * @param null $mapper
     * @return array
     * @throws \Exception
     */
    protected function transform($data, &$mapper = null)
    {
        if ($data instanceof \SplFileInfo) {
            return ImportSpreadsheetToData::fileToData($data, $this->map_keys, $mapper);
        }
        if ($data instanceof Collection) {
            return array_build_map($data->all(), $this->map_keys, $mapper);
        }
        if (is_array($data)) {
            return array_build_map($data, $this->map_keys, $mapper);
        }
        throw new \Exception('Invalid data type');
    }

    /**
     * @param $file \SplFileInfo
     */
    protected function headerRow($file)
    {
        return head(head($this->spreadsheetToArray($file)));
    }
}
