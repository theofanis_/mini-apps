<?php

namespace App\Exports;

use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ExportAverageEmployeesAnnually implements WithHeadings, FromArray, WithTitle, WithStyles, ShouldAutoSize
{
    use Exportable;
    use ExportUtils;

    /**
     * @var Carbon
     */
    public $start_of_year;
    /**
     * @var Carbon
     */
    public $end_of_year;
    /**
     * @var array|\SplFileInfo
     */
    public $data;

    public $map_keys = [
        'name'  => ['name', 'onoma'],
        'begin' => ['begin', '*prosl*', 'enarksi', 'apo'],
        'end'   => ['end', '*apokh*', '*apox*', 'liksi', 'eos'],
    ];

    /**
     * @param Collection|\SplFileInfo $data
     * @param int|null $year
     */
    public function __construct($data, $year = null)
    {
        $this->data = $data;
        $this->start_of_year = now()->startOfYear();
        $this->end_of_year = now()->endOfYear();
        if (!empty($year)) {
            $this->start_of_year = $this->start_of_year->year($year);
            $this->end_of_year = $this->end_of_year->year($year);
        }
    }

    public function map($row): array
    {
        if (empty($row['name'])) {
            return [];
        }
        $begin = new Carbon($this->parseDate($row['begin']));
        $begin = $begin->isBefore($this->start_of_year) ? $this->start_of_year : $begin;
        if (empty($row['end'])) {
            $end = $this->end_of_year;
        } else {
            $end = new Carbon($this->parseDate($row['end']));
            $end = $end->isAfter($this->end_of_year) ? $this->end_of_year : $end;
        }
        $months = $end->floatDiffInRealMonths($begin);
        $months_av = $months / 12;
        return [
            $row['name'],
            $row['begin'],
            $row['end'],
            round($months, 2),
            round($months_av, 2)
        ];
    }

    protected function footer()
    {
        return [
            __('Sum'),
            '',
            '',
            array_sum(array_column($this->data, 3)),
            array_sum(array_column($this->data, 4)),
        ];
    }

    public function array(): array
    {
        $data = $this->transform($this->data);
        $this->data = [];
        foreach ($data as $row) {
            $this->data[] = $this->map($row);
        }
        $this->data[] = $this->footer();
        return $this->data;
    }


    public function headings(): array
    {
        return [
            array_map('humanize_attr', ['name', 'begin', 'end', 'months', 'months average']),
        ];
    }

    public function title(): string
    {
        return __("Average Employees For :year", ['year' => $this->start_of_year->year]);
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle(1)->getFont()->setBold(true);
        $sheet->getStyle($sheet->getHighestRow())->getFont()->setBold(true);
    }
}
