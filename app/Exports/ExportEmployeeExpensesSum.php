<?php

namespace App\Exports;

use App\Imports\ImportSpreadsheetToData;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Facades\Excel;

class ExportEmployeeExpensesSum implements WithMultipleSheets
{
    use Exportable;
    use ExportUtils;

    /**
     * @var array|Collection
     */
    public $data;
    /**
     * @var \SplFileInfo
     */
    public $file;
    /**
     * @var array
     */
    public $totals;
    /**
     * @var string[]
     */
    public $mapper;

    public $map_keys = [
        'name' => ['name', 'onomatepwnimo', 'onoma', 'epwnimo', 'eponymo', 'onoma*'],
        'date' => ['date', 'hmernia_aksiologhisis', 'hmernia*', 'hmerominia*'],
    ];

    /**
     * @param \SplFileInfo $file
     */
    public function __construct($file)
    {
        $this->file = $file;
    }

    public function parse()
    {
        $this->transform($this->file, $this->mapper);
        $this->data = Excel::toArray(new ImportSpreadsheetToData(null), $this->file);
        $this->data = collect(last($this->data));

        $this->data = $this->data->map(function ($row, $index) {
            $row['days'] = $this->parseDates($row[$this->mapper['date']]);
            return $row;
        });

        $this->calculateTotals();

        $heading = $this->headerRow($this->file);
        $heading['days'] = __('Computed Days');
        $this->data->prepend($heading);

        $this->data = $this->data->map(function ($row) {
            return collect($row)->values()->all();
        })->all();

        return $this->data;
    }

    protected function parseDates($cell)
    {
        if (blank($cell))
            return 0;
        $dates = preg_split("/[&,-]/", $cell);
        return count($dates);
    }

    protected function calculateTotals()
    {
        $this->totals = $this->data->groupBy($this->mapper['name'])->map(function ($rows, $name) {
            if (empty($name))
                return null;
            return [
                'name'    => $name,
                'records' => count($rows),
                'days'    => array_sum(array_column($rows->all(), 'days'))
            ];
        })->filter();
        $this->totals->push([
            'name'    => __('Sum'),
            'records' => $this->totals->sum->records,
            'days'    => $this->totals->sum->days,
        ]);
        $this->totals->prepend([
            'name'    => __('Name'),
            'records' => __('Records'),
            'days'    => __('Total Days'),
        ]);
        $this->totals = $this->totals->values()->map(function ($row) {
            return array_values($row);
        })->all();
        return $this->totals;
    }

    public function sheets(): array
    {
        if (empty($this->data))
            $this->parse();
        return [
            SimpleExport::make($this->data, __('Records'))->withBold(1),
            SimpleExport::make($this->totals, __('Totals'))->withBold([
                1                    => ['font' => ['bold' => true]],
                count($this->totals) => ['font' => ['bold' => true]],
            ]),
        ];
    }
}
