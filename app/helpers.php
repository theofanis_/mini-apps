<?php
/**
 * helpers.php
 * User: Theofanis V. (vardtheo@gmail.com)
 * Date: 23/6/20 9:10 μ.μ.
 */

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Laravel\Telescope\Telescope;

if (!function_exists('humanize')) {

    /**
     * @param object|string $object Class or object to translate
     * @return string
     */
    function humanize($object)
    {
        return __(title_case(snake_case(class_basename($object), ' ')));
    }
}

if (!function_exists('humanize_attr')) {

    /**
     * @param string $attr Attribute in snake_case to translate
     * @return string
     */
    function humanize_attr($attr)
    {
        return __(ucwords(str_replace(['-', '_'], ' ', $attr)));
    }
}

if (!function_exists('is_assoc')) {
    /**
     * @param array $arr
     * @return bool - if the array is associative
     */
    function is_assoc(array $arr)
    {
        if (array() === $arr) return false;
        return array_keys($arr) !== range(0, count($arr) - 1);
    }
}

if (!function_exists('valid_file')) {
    /**
     * @param        $file
     * @param string $disk Empty disk means no disk but the file param has complete path.
     * @return bool tru if file parameter is filled, file exists and its size is larger than 1
     */
    function valid_file($file, $disk = null)
    {
        if (empty($file))
            return false;
        if (!empty($disk))
            $file = \Illuminate\Support\Facades\Storage::disk($disk)->path($file);
        return file_exists($file) && filesize($file);
    }
}

if (!function_exists('while_logged_in')) {
    /**
     * Login as the given user if there is not one already logged in, execute the callback and logout only if a login happened from this function.
     * @param callable $callback
     * @param User $user As which user to login
     * @return mixed Result of callback
     */
    function while_logged_in(callable $callback, User $user)
    {
        if (!($wasLogged = Auth::check()))
            Auth::login($user);
        $result = $callback();
        if (!$wasLogged)
            Auth::logout();
        return $result;
    }
}

if (!function_exists('log_step')) {
    /**
     * Log current step of process for debugging performance.
     * @param string $step
     * @param string|null $level
     * @param array $context
     */
    function log_step($step, $level = 'debug', $context = [])
    {
        static $last = LARAVEL_START;
        $now = microtime(true);
        $previous = floor(($now - $last) * 1000);
        $total = floor(($now - LARAVEL_START) * 1000);
        Log::log($level ?? 'debug', "$step +{$previous}ms - {$total}ms", $context);
        $last = $now;
    }
}

if (!function_exists('public_url')) {
    /**
     * @param string $url Replaces the locally defined app url (usually http://192.168.1.77) with the actual url of this request (e.g. https://79.129.22.252)
     * @return string
     */
    function public_url($url)
    {
        return str_replace(config('app.url'), request()->getSchemeAndHttpHost(), $url);
    }
}

if (!function_exists('sign')) {
    /**
     * Return the sign of the given number.
     * If number is 0 returns the zero parameter.
     * @param float|int $n
     * @param int $zero
     * @return float|int
     */
    function sign($n, $zero = 0)
    {
        return $n ? ($n > 0 ? 1 : -1) : $zero;
    }
}

if (!function_exists('filesize_formatted')) {
    /**
     * @param string|int|float $file For string path of file to calculate, otherwise number of bytes to format.
     * @param int $decimals
     * @return string
     */
    function filesize_formatted($file, $decimals = 2)
    {
        $bytes = is_string($file) ? filesize($file) : $file;
        $size = array('B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
    }
}

if (!function_exists('parse_money')) {
    /**
     * @param null|string|int|float $money Money from json
     * @return string
     */
    function parse_money($money)
    {
        return is_numeric($money) ? $money / 100 : $money;
    }
}

/**
 * @param array data array
 * @param string[]|string[][] $keys Array of new keys pointing to existing columns of the passed array.
 *                       Multiple values may be specified for the same key as fallback.
 * @param string[]|null $mapper Optionally returns the mapped keys to columns
 * @return array
 */
function array_build_map($array, $keys, &$mapper = null)
{
    if (empty($array) || empty($keys))
        return [];

    // setup keys mapper
    $mapper = [];
    $headers = array_keys(head($array));
    foreach ($keys as $key => $columns) {
        if (is_array($columns)) {
            foreach ($columns as $column) {
                foreach ($headers as $header) {
                    if (Str::is($column, $header)) {
                        $mapper[$key] = $header;
                        break;
                    }
                }
                if (!empty($mapper[$key]))
                    break;
            }
        } else {
            $mapper[$key] = $columns;
        }
        throw_if(empty($mapper[$key]), \App\Exceptions\InvalidDataException::class, "No column found for key '$key'");
    }

    // parse array into transformed
    $data = [];
    foreach ($array as $row) {
        $t = [];
        foreach ($mapper as $key => $column) {
            $t[$key] = $row[$column];
        }
        $data[] = $t;
    }

    return $data;
}

function telescope_exception()
{
    // \Facade\Ignition\ErrorPage\ErrorPageViewModel::telescopeUrl
    try {
        if (!class_exists(Telescope::class)) {
            return null;
        }

        if (!count(Telescope::$entriesQueue)) {
            return null;
        }

        $telescopeEntry = collect(Telescope::$entriesQueue)->first(function (\Laravel\Telescope\IncomingEntry $entry) {
            return $entry->type == 'exception';
        });

        if (is_null($telescopeEntry)) {
            return null;
        }

        $telescopeEntryId = (string)$telescopeEntry->uuid;
        return $telescopeEntryId;
        //return url(action([Laravel\Telescope\Http\Controllers\HomeController::class, 'index']) . "/exceptions/{$telescopeEntryId}");
    } catch (Exception $exception) {
        report($exception);
        return null;
    }
}

/**
 * Store Telescope::$entriesQueue
 */
function telescope_store()
{
    Telescope::store(app(\Laravel\Telescope\Contracts\EntriesRepository::class));
}

/**
 * @param \Illuminate\Database\Eloquent\Model|array|mixed $model
 * @param string $attr
 */
function model_id($model, $attr = null)
{
    if ($model instanceof \Illuminate\Database\Eloquent\Model) {
        return $attr ? $model->{$attr} : $model->getKey();
    }
    if (is_array($model)) {
        return $model[$attr ?? 'id'];
    }
    return $model;
}

/**
 * @param array $array
 * @param string[] $keys
 * @param bool $notEmpty
 * @return mixed|null
 */
function array_first_by_key($array, $keys, $notEmpty = false)
{
    foreach ($keys as $key) {
        if ($notEmpty ? !empty($array[$key]) : isset($array[$key])) {
            return $array[$key];
        }
    }
    return null;
}

/**
 * @param mixed $value
 * @return int|null
 */
function intOrNull($value)
{
    return is_numeric($value) || $value ? (int)$value : null;
}

function debug_db($connection = null, $log_channel = 'stdout')
{
    DB::connection($connection)->listen(function (\Illuminate\Database\Events\QueryExecuted $query) use ($log_channel) {
        \Illuminate\Support\Facades\Log::channel($log_channel)
            ->debug("database.connections.{$query->connectionName} query {$query->time}ms: {$query->sql}", $query->bindings);
    });
}

/**
 * @param callable $callback
 * @param int|float $duration Duration in milliseconds
 * @return mixed
 */
function timed(callable $callback, &$duration)
{
    $start = microtime(true);
    $result = call_user_func($callback);
    $end = microtime(true);
    $duration = floor(($end * 1000 - $start * 1000));
    return $result;
}

/**
 * @param mixed $datum
 * @return string
 */
function toString($datum)
{
    return (string)$datum;
}

/**
 * @param mixed $value
 * @return Closure
 */
function toClosure($value): Closure
{
    return is_callable($value)
        ? \Closure::fromCallable($value)
        : function () use ($value) {
            return value($value);
        };
}

/**
 * @param numeric $number
 * @return float the number rounded with 2 decimals
 */
function n2d($number): float
{
    return (float)(string)round($number, 2);
}

/**
 * Recursively implodes an array with optional key inclusion
 *
 * Example of $include_keys output: key, value, key, value, key, value
 *
 * @access  public
 * @param array $array multi-dimensional array to recursively implode
 * @param string $glue value that glues elements together
 * @param bool $include_keys include keys before their values
 * @param bool $trim_all trim ALL whitespace from string
 * @return  string  imploded array
 */
function recursive_implode(array $array, $glue = ',', $include_keys = false, $trim_all = true)
{
    $glued_string = '';

    // Recursively iterates array and adds key/value to glued string
    array_walk_recursive($array, function ($value, $key) use ($glue, $include_keys, &$glued_string) {
        $include_keys and $glued_string .= $key . $glue;
        $glued_string .= $value . $glue;
    });

    // Removes last $glue from string
    strlen($glue) > 0 and $glued_string = substr($glued_string, 0, -strlen($glue));

    // Trim ALL whitespace
    $trim_all and $glued_string = preg_replace("/(\s)/ixsm", '', $glued_string);

    return (string)$glued_string;
}
