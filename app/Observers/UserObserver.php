<?php

namespace App\Observers;

use App\Models\User;

class UserObserver
{
    public function deleted(User $user)
    {
        $user->email = str_finish($user->email, "[deleted-{$user->id}]");
        $user->saveQuietly();
    }

    public function restored(User $user)
    {
        $user->email = str_before($user->email, "[deleted-{$user->id}]");
        $user->saveQuietly();
    }
}
