<?php

namespace App\Http\Controllers\Apps;

use App\Exports\ExportAverageEmployeesAnnually;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AccountingController extends Controller
{
    public function averageEmployeesAnnually(Request $request)
    {
        $request->validate([
            'year'      => 'required|digit:4',
            'employees' => 'required|file',
        ]);
        $year = $request->input('year', now()->year);
        $file = $request->file('employees');
        return (new ExportAverageEmployeesAnnually($file, $year))->download("{$file->getBasename()}-export.{$file->extension()}");
    }
}
