<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Session;
use Livewire\Component;

class CookiePolicy extends Component
{
    public $showBanner = false;

    public function mount()
    {
        $this->showBanner = auth()->guest() && !session('accepted-cookie-policy');
    }

    public function accept()
    {
        Session::put('accepted-cookie-policy', true);
        $this->showBanner = false;
    }

    public function render()
    {
        return view('livewire.cookie-policy');
    }
}
