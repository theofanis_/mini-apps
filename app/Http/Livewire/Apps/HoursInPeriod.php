<?php

namespace App\Http\Livewire\Apps;

use App\Exports\ExportHoursInPeriod;

class HoursInPeriod extends AppComponent
{

    public $period_begin;
    public $period_end;
    public $file;
    public $validated = false;

    public $rules = [
        'period_begin' => ['required', 'time'],
        'period_end'   => ['required', 'time'],
        'file'         => ['required', 'spreadsheet'],
    ];

    public $validationAttributes = [
        'file'         => 'file',
        'period_begin' => 'period_begin',
        'period_end'   => 'period_end',
    ];

    public function mount()
    {
        $this->translateValidationAttributes();
        $this->rememberProperties(['period_begin' => '22:00', 'period_end' => '6:00']);
    }

    public function updatedFile()
    {
        $this->validateAndFlag();
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
        $this->storeIfRemembered($propertyName);
    }

    public function submit()
    {
        $this->validateAndFlag();
        return $this->respondSafely(function () {
            $e = new ExportHoursInPeriod($this->file, $this->period_begin, $this->period_end);
            return tap($e->download($this->exportableFilename($this->file, $e)), function () {
                $this->storeProperties();
            });
        });
    }

    public function render()
    {
        return view('livewire.apps.hours-in-period');
    }
}
