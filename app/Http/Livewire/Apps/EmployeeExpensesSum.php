<?php

namespace App\Http\Livewire\Apps;

use App\Exports\ExportEmployeeExpensesSum;
use Livewire\TemporaryUploadedFile;

class EmployeeExpensesSum extends AppComponent
{

    /**
     * @var TemporaryUploadedFile
     */
    public $file;
    public $validated = false;

    public $rules = [
        'file' => ['required', 'spreadsheet'],
    ];

    public $validationAttributes = [
        'file' => 'spreadsheet',
    ];

    public function updatedFile()
    {
        $this->validateAndFlag();
    }

    public function mount()
    {
        $this->translateValidationAttributes();
    }

    public function submit()
    {
        $this->validateAndFlag();
        return $this->respondSafely(function () {
            $e = new ExportEmployeeExpensesSum($this->file);
            return $e->download($this->exportableFilename($this->file, $e));
        });
    }

    public function render()
    {
        return view('livewire.apps.employee-expenses-sum');
    }
}
