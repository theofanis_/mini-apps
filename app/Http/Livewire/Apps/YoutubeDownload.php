<?php

namespace App\Http\Livewire\Apps;

use GuzzleHttp\Psr7\MimeType;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\ValidationException;
use Laravel\Telescope\Telescope;
use Symfony\Component\Mime\MimeTypes;
use Symfony\Component\Process\Process;
use YouTube\Exception\VideoNotFoundException;
use YouTube\Models\StreamFormat;
use YouTube\YouTubeDownloader;

class YoutubeDownload extends AppComponent
{
    public $youtube_link = null;
    public $youtube_video_title = null;
    public $formats = [];
    public $format = null;
    public $validated = false;

    public $rules = [
        'youtube_link' => ['required'],
        'format'       => ['required_with:youtube_link'],
    ];

    public function mount()
    {
        $this->translateValidationAttributes();
        $this->rememberProperties(['format' => $this->format, 'youtube_link' => $this->youtube_link ?? 'https://www.youtube.com/watch?v=dQw4w9WgXcQ']);
        $this->updateFormatOptions();
        $this->validateAndFlag();
    }

    public function updated($propertyName)
    {
        $this->storeIfRemembered($propertyName);
        $this->validateAndFlag();
    }

    public function updatingYoutubeLink($value)
    {
        $this->youtube_link = trim($value);
        $this->storeProperties(['youtube_link' => $this->youtube_link]);
        $this->updateFormatOptions();
    }

    private function updateFormatOptions()
    {
        if (blank($this->youtube_link)) {
            $this->reset();
            return;
        }
        $phpFormats = $this->getFormatOptionsUsingPHP();
        $youtubeDlFormats = $this->getFormatOptionsUsingYoutubeDl();
        $this->formats = array_merge($phpFormats, $youtubeDlFormats);
        $this->format = $this->format ?: head($phpFormats)['code'] ?? null;
    }

    private function getFormatOptionsUsingPHP()
    {
        try {
            $downloadOptions = (new YouTubeDownloader())->getDownloadLinks($this->youtube_link);
        } catch (VideoNotFoundException $e) {
            $this->resetExcept('youtube_link');
            throw ValidationException::withMessages(['youtube_link' => "Video not found"]);
        }
        $this->youtube_video_title = $downloadOptions->getInfo()->getTitle();
        return collect($downloadOptions->getAllFormats())
            ->map(fn(StreamFormat $f) => ['name' => "{$f->getCleanMimeType()} {$f->qualityLabel} " . strtolower($f->audioQuality), 'code' => "{$f->itag}", 'url' => $f->url, 'downloader' => 'php'])
            ->toArray();
    }

    private function getFormatOptionsUsingYoutubeDl()
    {
        return [
            ['name' => 'video mp4', 'code' => 'mp4', 'type' => 'video', 'downloader' => 'youtube-dl'],
            ['name' => 'video 3gp', 'code' => '3gp', 'type' => 'video', 'downloader' => 'youtube-dl'],
            ['name' => 'video flv', 'code' => 'flv', 'type' => 'video', 'downloader' => 'youtube-dl'],
            ['name' => 'video webm', 'code' => 'webm', 'type' => 'video', 'downloader' => 'youtube-dl'],
            ['name' => 'audio mp3', 'code' => 'mp3', 'type' => 'audio', 'downloader' => 'youtube-dl'],
            ['name' => 'audio aac', 'code' => 'aac', 'type' => 'audio', 'downloader' => 'youtube-dl'],
            ['name' => 'audio m4a', 'code' => 'm4a', 'type' => 'audio', 'downloader' => 'youtube-dl'],
            ['name' => 'audio ogg', 'code' => 'ogg', 'type' => 'audio', 'downloader' => 'youtube-dl'],
            ['name' => 'audio wav', 'code' => 'wav', 'type' => 'audio', 'downloader' => 'youtube-dl'],
        ];
    }

    public function submit()
    {
        $this->validateAndFlag();
        $this->storeProperties();
        return $this->respondSafely(fn() => $this->downloadVideo());
    }

    private function downloadVideo()
    {
        return $this->getFormatConfig()['downloader'] == 'php'
            ? $this->downloadVideoUsingPHP()
            : $this->downloadVideoUsingYoutubeDl();
    }

    private function downloadVideoUsingPHP()
    {
        $downloadOptions = (new YouTubeDownloader())->getDownloadLinks($this->youtube_link);
        /** @var StreamFormat $streamFormat */
        $streamFormat = collect($downloadOptions->getAllFormats())
            ->firstWhere('itag', $this->format);
        $extension = head(MimeTypes::getDefault()->getExtensions($streamFormat->getCleanMimeType()));
        $filename = "{$this->youtube_video_title}.$extension";
        $url = $streamFormat->url;
        $tmpFile = tempnam(sys_get_temp_dir(), 'video');
        $this->log("Downloading '$url' to $tmpFile");
        telescope_store();
        copy($url, $tmpFile);

        $this->log("Returning from php '$tmpFile' as '$filename'");
        return response()->download($tmpFile, $filename);
    }

    private function downloadVideoUsingYoutubeDl()
    {
        $dir = "/tmp/" . now()->format('Uu');
        $env = ['youtube_link' => $this->youtube_link, 'format' => $this->format];
        $typeConfig = $this->getFormatConfig();
        $cmd = "PATH='" . config('mini-apps.extend_path') . "' ";
        $cmd .= $typeConfig['type'] == 'audio'
            ? "youtube-dl --extract-audio --audio-format \$format --audio-quality 2 --ignore-errors --no-overwrites --no-post-overwrites --embed-thumbnail --xattrs --add-metadata --metadata-from-title \"%(artist)s - %(title)s\" --console-title -o \"$dir/%(title)s.%(ext)s\" \"\$youtube_link\""
            : "youtube-dl -f \$format -o \"$dir/%(title)s.%(ext)s\" \"\$youtube_link\"";

        $this->log($cmd, $env);
        telescope_store();
        $p = Process::fromShellCommandline($cmd);
        $p->setTimeout(60 * 10);
        $p->setEnv($env);
        $p->mustRun();

        $file = head(File::files($dir));
        $this->log("Returning from youtube-dl '$file'");
        return response()->download($file);
    }

    public function getFormatConfigProperty()
    {
        return $this->getFormatConfig() ?? [];
    }

    private function getFormatConfig()
    {
        return collect($this->formats)->firstWhere('code', $this->format);
    }

    public function render()
    {
        return view('livewire.apps.youtube-download');
    }
}
