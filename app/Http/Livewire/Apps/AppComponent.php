<?php

namespace App\Http\Livewire\Apps;

use App\Exceptions\AppUsageException;
use App\Exceptions\BaseException;
use App\Exports\ExportUtils;
use App\Models\MiniApp;
use App\Models\MiniAppUsage;
use Illuminate\Support\Facades\Session;
use Livewire\Component;
use Livewire\WithFileUploads;

abstract class AppComponent extends Component
{
    use WithFileUploads;

    /**
     * Properties to save in session
     * @var array
     */
    public $rememberProperties = [];

    public $validationAttributes = [];
    public $validated = false;

    /**
     * @param array|null $properties If null then loads properties from $rememberProperties
     */
    protected function rememberProperties($properties = null)
    {
        if (!is_null($properties)) {
            $this->rememberProperties = $properties;
        }
        foreach ($this->rememberProperties as $name => $default) {
            // if it does not have default value then key would be null
            if (is_int($name)) {
                unset($this->rememberProperties[$name]);
                $name = $default;
                $default = null;
                $this->rememberProperties[$name] = $default;
            }
            $this->{$name} = Session::get("app.{$this->getAppCode()}.$name", $default);
        }
    }

    /**
     * @param null|array|string $properties If null then loads properties from $rememberProperties
     */
    protected function storeProperties($properties = null)
    {
        if (is_null($properties)) {
            $properties = $this->rememberProperties;
        } else {
            $properties = array_wrap($properties);
        }
        foreach ($properties as $name => $value) {
            // if it does not have value load from current property
            if (is_int($name)) {
                $name = $value;
                $value = $this->{$name};
            }
            Session::put("app.{$this->getAppCode()}.$name", $value);
        }
    }

    /**
     * @param string $propertyName
     * @return bool
     */
    protected function remembersProperty($propertyName)
    {
        return array_key_exists($propertyName, $this->rememberProperties);
    }

    protected function validateAndFlag()
    {
        $this->validated = false;
        return tap(parent::validate(), function () {
            $this->validated = true;
        });
    }

    /**
     * @param string $propertyName
     * @return bool
     */
    protected function storeIfRemembered($propertyName)
    {
        if ($this->remembersProperty($propertyName)) {
            $this->storeProperties($propertyName);
            return true;
        }
        return false;
    }

    /**
     * @param \SplFileInfo $file
     * @return string
     */
    protected function filename($file)
    {
        return pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
    }

    /**
     * @param \SplFileInfo $file
     * @param ExportUtils $exportable
     */
    protected function exportableFilename(\SplFileInfo $file, $exportable)
    {
        $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        return "$filename-{$exportable->title()}.{$file->getClientOriginalExtension()}";
    }

    protected function translateValidationAttributes()
    {
        $this->validationAttributes = array_map('humanize_attr', $this->validationAttributes);
    }

    protected function translateMessages()
    {
        $this->messages = array_map('humanize', $this->messages);
    }

    protected function respondSafely(callable $callback)
    {
        try {
            return tap(timed($callback, $duration), fn() => $this->recordUsage(['duration' => $duration]));
        } catch (BaseException $e) {
            throw $e;
        } catch (\Throwable $e) {
            if (config('app.debug')) {
                throw $e;
            }
            report($e);
            throw new \Exception(__('Something went wrong.'), 0, $e);
        }
    }

    /**
     * @param array $info
     * @return MiniAppUsage
     */
    protected function recordUsage($info = [])
    {
        try {
            return $this->getMiniApp()->recordUsage($info);
        } catch (\Throwable $e) {
            report((new AppUsageException("Failed to record usage: {$this->getAppCode()}", 0, $e))->withContext(['component' => static::class]));
        }
        return null;
    }

    /**
     * @return MiniApp
     */
    public function getMiniApp()
    {
        return MiniApp::findByCode($this->getAppCode());
    }

    /**
     * @return string
     */
    public function getAppCode()
    {
        return kebab_case(class_basename(static::class));
    }

    public function log($message, $context = [])
    {
        log_step("[{$this->getAppCode()}] $message", null, $context);
    }

}
