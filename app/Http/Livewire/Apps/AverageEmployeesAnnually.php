<?php

namespace App\Http\Livewire\Apps;

use App\Exports\ExportAverageEmployeesAnnually;
use Livewire\TemporaryUploadedFile;

class AverageEmployeesAnnually extends AppComponent
{

    /**
     * @var TemporaryUploadedFile
     */
    public $file;
    /**
     * @var int
     */
    public $year;
    public $validated = false;

    public $rules = [
        'year' => ['required', 'digits:4'],
        'file' => ['required', 'spreadsheet'],
    ];

    public $validationAttributes = [
        'file' => 'file',
        'year' => 'year',
    ];

    public function updatedFile()
    {
        $this->validateAndFlag();
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
        $this->storeIfRemembered($propertyName);
    }

    public function mount()
    {
        $this->translateValidationAttributes();
        $this->rememberProperties(['year' => now()->year - 1]);
    }

    public function submit()
    {
        $this->validateAndFlag();
        return $this->respondSafely(function () {
            $e = new ExportAverageEmployeesAnnually($this->file, $this->year);
            return tap($e->download($this->exportableFilename($this->file, $e)), function () {
                $this->storeProperties();
            });
        });
    }

    public function render()
    {
        return view('livewire.apps.average-employees-annually');
    }
}
