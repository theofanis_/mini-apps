<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SetupPath extends Command
{
    protected $signature = 'path:setup';

    protected $description = 'Command description';

    public function handle()
    {
        $path = env('PATH');
        $var = "EXTEND_PATH=$path";
        $file = app()->environmentFilePath();

        $data = file_get_contents($file);
        $data = preg_replace("/^EXTEND_PATH.*/m", $var, $data);
        if (preg_match("/^EXTEND_PATH/m", $data) < 1) {
            $data .= "\n$var\n";
        }
        file_put_contents($file, $data);

        $this->line("Successfully setup EXTEND_PATH env variable.");
        return 0;
    }
}
