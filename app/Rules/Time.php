<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Validation\Concerns\FormatsMessages;
use Illuminate\Validation\Concerns\ValidatesAttributes;

class Time implements Rule
{
    use FormatsMessages, ValidatesAttributes;

    private $attribute;

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @param string $parameters
     * @return bool
     */
    public function passes($attribute, $value, $parameters = [])
    {
        $this->attribute = $attribute;
        return $this->validateDateFormat($attribute, $value, $parameters ?: ['G:i']);
    }

    public function message()
    {
        return $this->getMessage($this->attribute, 'time');
    }
}
