<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Validation\Concerns\ValidatesAttributes;

class Spreadsheet implements Rule
{
    use ValidatesAttributes;

    public const EXTENSIONS = ['xls', 'xlsx', 'ods', 'csv', 'tsv'];
//    protected $attribute;

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
//        $this->attribute = $attribute;
        return $this->validateMimes($attribute, $value, self::EXTENSIONS);
    }

    public function message()
    {
//        return trans('validation.mimes', ['values' => self::EXTENSIONS]);
        return trans('validation.mimes');
    }
}
