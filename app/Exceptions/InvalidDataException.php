<?php

namespace App\Exceptions;

class InvalidDataException extends BaseException
{
    public $status = 500;

}
