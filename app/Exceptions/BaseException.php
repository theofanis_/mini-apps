<?php

namespace App\Exceptions;


use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Support\Collection;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Throwable;

/**
 * @property string $exception
 * @property string $message
 * @property string $localized_message
 * @package App\Exceptions
 */
abstract class BaseException extends \Exception implements Arrayable, \JsonSerializable, Jsonable, \Serializable, HttpExceptionInterface
{
    protected $attributes = [];
    /**
     * HTTP status code
     * @var int
     */
    public $status = 503;
    /**
     * @var array
     */
    public $context = [];
    /**
     * @var array
     */
    public $headers = [];

    public function __get($name)
    {
        return property_exists($this, $name) ? $this->{$name} : $this->attributes[$name];
    }

    public function __set($name, $value)
    {
        if (property_exists($this, $name)) {
            $this->{$name} = $value;
        } else {
            $this->attributes[$name] = $value;
        }
    }

    /**
     * @param array|Collection $attributes
     * @return static
     */
    public static function make($attributes)
    {
        $instance = new static();
        foreach ($attributes as $k => $v)
            $instance->__set($k, $v);
        return $instance;
    }

    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        $this->exception = class_basename($this);
        $this->attributes['message'] = $this->localized_message = $this->getLocalizedMessage();
        $this->attributes['code'] = $code;
        if (filled($message))
            $this->attributes['message'] = $message;
        parent::__construct($this->attributes['message'], $code, $previous);
    }

    public function getLocalizedMessage()
    {
        return __("exception." . str_before(class_basename($this), "Exception"), $this->getAttributes());
    }

    public function toArray()
    {
        return $this->attributes;
    }

    public function getAttributes()
    {
        return $this->attributes;
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }

    /**
     * @param int $options
     * @return false|string
     * @throws \Exception
     */
    public function toJson($options = 0)
    {
        $json = json_encode($this->jsonSerialize(), $options);
        if (JSON_ERROR_NONE !== json_last_error())
            throw new \Exception('Error encoding exception [' . get_class($this) . ']  to JSON: ' . json_last_error_msg());
        return $json;
    }

    public function serialize()
    {
        return serialize($this->toArray());
    }

    public function unserialize($serialized)
    {
        $this->attributes = unserialize($serialized);
    }

    public function getStatusCode()
    {
        return $this->status;
    }

    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param array $context
     * @return $this
     */
    public function withContext($context)
    {
        $this->context = $context;
        return $this;
    }

    public function context()
    {
        return $this->context;
    }
}
