<?php

namespace App\Exceptions;

use Illuminate\Support\Facades\Log;

class AppUsageException extends BaseException
{
    public function report()
    {
        Log::channel('slack')->emergency($this->getMessage(), $this->context());
    }
}
