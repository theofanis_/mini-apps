<?php

namespace App\Imports;

use App\Exceptions\InvalidDataException;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToArray;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Facades\Excel;

class ImportSpreadsheetToData implements WithHeadingRow, ToArray
{
    use Importable;

    public $map_keys = [];
    public $keys = [];
    public $data = [];

    public static function fileToData($file, $map_keys, &$mapper = null)
    {
        Excel::import($b = new static($map_keys), $file);
        $mapper = $b->keys;
        return $b->data;
    }

    public function __construct($map_keys)
    {
        $this->map_keys = $map_keys;
    }

    public function array(array $array)
    {
        if (empty($row = head($array))) {
            $this->keys = [];
            return $this->data = [];
        }

        $this->setupKeys($row);

        $this->data = [];
        foreach ($array as $row) {
            $t = [];
            foreach ($this->keys as $key => $column) {
                $t[$key] = $row[$column];
            }
            $this->data[] = $t;
        }

        return $this->data;
    }

    /**
     * @param $row
     * @throws \Throwable
     */
    protected function setupKeys($row)
    {
        $headers = array_keys($row);
        foreach ($this->map_keys as $key => $columns) {
            if (is_array($columns)) {
                foreach ($columns as $column) {
                    foreach ($headers as $header) {
                        if (Str::is($column, $header)) {
                            $this->keys[$key] = $header;
                            break;
                        }
                    }
                    if (!empty($this->keys[$key])) {
                        break;
                    }
                }
            } else {
                $this->keys[$key] = $columns;
            }
            throw_if(empty($this->keys[$key]), InvalidDataException::class, __("Unknown column for field :field.", ['field' => $key]));
        }
    }

}
