<?php

namespace App\Providers;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function register()
    {
        //
    }

    public function boot()
    {
        $this->registerMacros();
        $this->configureValidationRules();
        $this->discoverMiniApps();
    }

    public function discoverMiniApps()
    {
        $this->app->singleton('mini-apps', function () {
//            return Cache::tags('app')->remember('mini-apps', config('cache.duration'), function () {
            $modelNamespace = 'Http/Livewire/Apps';
            $appNamespace = \Illuminate\Container\Container::getInstance()->getNamespace();
            return collect(File::allFiles(app_path($modelNamespace)))->mapWithKeys(function (\SplFileInfo $item) use ($appNamespace, $modelNamespace) {
                $rel = $item->getRelativePathName();
                $class = sprintf('%s%s%s', $appNamespace, str_replace('/', '\\', $modelNamespace) . '\\',
                    str_replace('/', '\\', substr($rel, 0, strrpos($rel, '.'))));
                try {
                    $reflectionClass = new \ReflectionClass($class);
                    if ($reflectionClass->isAbstract() || !$reflectionClass->isSubclassOf(\App\Http\Livewire\Apps\AppComponent::class)) {
                        return [];
                    }
                    $obj = new $class;
                    return [$obj->getAppCode() => $reflectionClass->getName()];
                } catch (\ReflectionException $e) {
                    return [];
                }
            })->filter();
//            });
        });
    }

    private function configureValidationRules()
    {
        Validator::extend('spreadsheet', \App\Rules\Spreadsheet::class . '@passes');
        Validator::extend('time', \App\Rules\Time::class . '@passes');
    }

    private function registerMacros()
    {
        Blueprint::macro('blameable', function () {
            $this->foreignId(config('blameable.column_names.createdByAttribute'))->nullable();
            $this->foreignId(config('blameable.column_names.updatedByAttribute'))->nullable();
            $this->foreignId(config('blameable.column_names.deletedByAttribute'))->nullable();
        });
    }

}
