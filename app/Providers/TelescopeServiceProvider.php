<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Route;
use Laravel\Telescope\EntryType;
use Laravel\Telescope\IncomingEntry;
use Laravel\Telescope\Telescope;
use Laravel\Telescope\TelescopeApplicationServiceProvider;

class TelescopeServiceProvider extends TelescopeApplicationServiceProvider
{
    public function register()
    {
        // Telescope::night();
        $this->hideSensitiveRequestDetails();
        $this->tag();
        $this->filter();
    }

    private function tag()
    {
        Telescope::tag(function (IncomingEntry $entry) {
            switch ($entry->type) {
                case EntryType::REQUEST:
                    $tags = $this->tagRequest($entry);
                    break;
                case EntryType::EXCEPTION:
                    $tags = $this->tagException($entry);
                    break;
                case EntryType::LOG:
                    $tags = $this->tagLog($entry);
                    break;
                case "client_request":
                    $tags = $this->tagClientRequest($entry);
                    break;
                case EntryType::JOB:
                    $tags = $this->tagJob($entry);
                    break;
                default:
                    $tags = [];
            }

            $tags = collect($tags);
            $tags[] = now()->format('Y-m-d H');

            $request = request();
            //add to \App\Http\Middleware\EncryptCookies::$except
            $keys = ['TID'];

            foreach (['request', 'headers', 'cookies'] as $bag) {
                $tags = $tags->merge(
                    collect($request->$bag->all())
                        ->only($keys)
                        ->filter()
                        ->map(fn($v, $k) => "$k:$v")
                        ->values()

                );
            }

            return $tags;
        });
    }

    private function filter()
    {
        Telescope::filter(function (IncomingEntry $entry) {
            if ($this->app->environment('local')) {
                return true;
            }

            $filter = function () use ($entry) {
                switch ($entry->type) {
                    case EntryType::REQUEST:
                        if (array_get($entry->content, 'response_status') >= 400 || array_get($entry->content, 'duration') > 2500)
                            return true;
                        return !starts_with(array_get($entry->content, 'uri'), ['/nova-api/', '/nova-vendor/']);

                    case EntryType::SCHEDULED_TASK:
                        if (array_get($entry->content, 'description') == "Check status of required services")
                            return false;
                        return true;
                }
                return true;
            };

            return $entry->isReportableException() ||
                $entry->isFailedRequest() ||
                $entry->isFailedJob() ||
                $entry->isScheduledTask() ||
                $entry->hasMonitoredTag() ||
                $filter();
        });
    }

    protected function hideSensitiveRequestDetails()
    {
        if ($this->app->environment('local')) {
            return;
        }
        Telescope::hideRequestParameters(['_token']);
        Telescope::hideRequestHeaders([
            'cookie',
            'x-csrf-token',
            'x-xsrf-token',
        ]);
    }

    protected function gate()
    {
        Gate::define('viewTelescope', function (User $user) {
            return $user->can('super-admin');
        });
    }

    protected function tagRequest(IncomingEntry $entry)
    {
        $tags = collect([
            "status:{$entry->content['response_status']}",
            "method:{$entry->content['method']}",
            "ip:{$entry->content['ip_address']}",
        ]);

        if (array_get($entry->content, 'duration') > 2500) {
            $tags->push('slow');
        }

        if ($route = Route::current()) {

            if ($route->named('api.*')) {
                $tags->push($route->getName(), ...explode('.', $route->getName()));
            }

        }
        return $tags->toArray();
    }

    private function tagException(IncomingEntry $entry)
    {
        return [
            str_before(class_basename($entry->content['class']), 'Exception'),
        ];
    }

    private function tagLog(IncomingEntry $entry)
    {
        return [
            $entry->content['level'],
        ];
    }

    private function tagClientRequest(IncomingEntry $entry)
    {
        $tags = collect([
            "status:" . array_get($entry->content, 'response_status'),
            "method:" . array_get($entry->content, 'method'),
        ]);

        return $tags;
    }

    private function tagJob(IncomingEntry $entry)
    {
        return [
            class_basename(array_get($entry->content, 'name')),
        ];
    }

}
