<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;

class DefaultRolesWithPermissionsSeeder extends Seeder
{
    /**
     * @var Role
     */
    protected $superAdmin;
    /**
     * @var Role
     */
    protected $admin;

    public function run()
    {
        $this->setSuperAdminPermissions();
        $this->setAdminPermissions();
    }

    protected function setSuperAdminPermissions()
    {
        $this->superAdmin = Role::superAdmin()->syncPermissions(Permission::all());
    }

    protected function setAdminPermissions()
    {
        $permissions = Permission::query()
            ->whereNotIn('name', ['super-admin', 'run-commands'])
            ->where('name', 'not like', '% Role')
            ->where('name', 'not like', '% Permission')
            ->where('name', 'not like', '% ConfigModel')
            ->where('name', 'not like', '% User')
            ->get();
        $this->admin = Role::admin()->syncPermissions($permissions);
    }

}
