<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class DefaultUsersSeeder extends Seeder
{
    public function run()
    {
        $superAdmin = Role::superAdmin();
        User::system()->roles()->save($superAdmin);
        User::cron()->roles()->save($superAdmin);
        collect([
            ['name' => 'Theofanis', 'username' => 'theofanis', 'email' => 'vardtheo@gmail.com', 'plain_password' => 'Koukouts1.', 'roles' => Role::superAdmin()],
        ])->each(function ($attrs) {
            $roles = Arr::pull($attrs, 'roles');
            $u = User::create($attrs);
            $u->assignRole($roles);
        });
    }

}
