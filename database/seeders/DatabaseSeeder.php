<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Auth;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(SystemUserSeeder::class);
        Auth::loginUsingId(User::system()->id);
        $this->call(DefaultRolesSeeder::class);
        $this->call(DefaultPermissionsSeeder::class);
        $this->call(DefaultRolesWithPermissionsSeeder::class);
        $this->call(DefaultUsersSeeder::class);
        $this->call(MiniAppsSeeder::class);
        Auth::logout();
    }
}
