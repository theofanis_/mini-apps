<?php

namespace Database\Seeders;

use App\Models\MiniApp;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Cache;

class MiniAppsSeeder extends Seeder
{
    public function run()
    {
        Cache::tags('app')->forget('mini-apps');
        app('mini-apps')->each(function ($component, $code) {
            MiniApp::firstOrCreate(['code' => $code]);
        });
        Cache::tags('app')->forget('mini-apps');
    }
}
