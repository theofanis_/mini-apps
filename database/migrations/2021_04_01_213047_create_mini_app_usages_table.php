<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMiniAppUsagesTable extends Migration
{
    public function up()
    {
        Schema::create('mini_app_usages', function (Blueprint $table) {
            $table->id();
            $table->foreignId('mini_app_id');
            $table->foreign('mini_app_id')->references('id')->on('mini_apps');
            $table->string('mini_app_code')->index();
            $table->string('session')->nullable();
            $table->foreignId('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->json('info');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('app_usages');
    }
}
